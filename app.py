from flask import Flask, jsonify, request
import numpy as np
import cv2
import os

#import file
from response import success, badRequest

app = Flask(__name__)

@app.route('/train', methods=['GET'])
def train_route():
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    detector = cv2.CascadeClassifier('model/haarcascade_frontalface_default.xml')

    path = 'image'
    image_paths = [os.path.join(path, f) for f in os.listdir(path)]

    face_samples = []
    ids = []

    for image_path in image_paths:
        img = cv2.imread(image_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = detector.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5)

        if len(faces) == 0:
            return badRequest(data=0, message="No faces detected in image: {image_path}")

        for (x, y, w, h) in faces:
            face_samples.append(gray[y:y+h, x:x+w])
            ids.append(int(os.path.split(image_path)[1].split("-")[3].split(".")[0]))
            try:
                id = int(os.path.split(image_path)[1].split("-")[3].split(".")[0])
                ids.append(id)
            except IndexError as e:
                return badRequest(data=0, message="Error extracting ID from image path: {image_path}")

    if not face_samples:
        return badRequest(data=0, message="No valid face samples found. Training cannot proceed.")

    recognizer.train(face_samples, np.array(ids))
    recognizer.save("trained_model.yml")
    return jsonify({"message": "Training data completed."})

@app.route('/recognize', methods=['POST'])
def recognize_faces():
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read('trained_model.yml')
    
    detector = cv2.CascadeClassifier('model/haarcascade_frontalface_default.xml')
    
    if 'image' not in request.files:
        return jsonify({'error': 'file not found'})
    
    file = request.files['image']
    
    image = cv2.imdecode(np.fromstring(file.read(), np.uint8), cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    faces = detector.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

    if len(faces) == 0:
        return badRequest(data=0, message="Failed")
    else:
        for (x, y, w, h) in faces:
            id, predict = recognizer.predict(gray[y:y+h, x:x+w])
            confidence = "{0}%".format(round(100 - predict))
    
            if ((round(100 - predict)) > 50):
                return success(data=confidence, message="Success")
            else:
                return badRequest(data=0, message="Failed")

if __name__ == '__main__':
    app.run(debug=True)